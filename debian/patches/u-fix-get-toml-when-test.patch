Description: Fix get_toml() when cfg(test)
 When cfg(test), Config::parse doesn't parse a config.toml but uses default
 values, failing when the initial rustc is needed. This is a workaround before
 upstream issue gets solved.
Bug: https://github.com/rust-lang/rust/issues/105766
Last-Update: 2023-03-29
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/src/bootstrap/config.rs
+++ b/src/bootstrap/config.rs
@@ -823,9 +823,9 @@
     }
 
     pub fn parse(args: &[String]) -> Config {
-        #[cfg(test)]
+        /*#[cfg(test)]
         let get_toml = |_: &_| TomlConfig::default();
-        #[cfg(not(test))]
+        #[cfg(not(test))]*/
         let get_toml = |file: &Path| {
             let contents =
                 t!(fs::read_to_string(file), format!("config file {} not found", file.display()));
@@ -834,7 +834,23 @@
             match toml::from_str(&contents)
                 .and_then(|table: toml::Value| TomlConfig::deserialize(table))
             {
-                Ok(table) => table,
+                // Debian: We use previous version as a custom rustc, which
+                // unfortunately won't be picked up because config.toml isn't
+                // read when cfg!(test). Making tests use the entirety of our
+                // config.toml isn't feasible either as it panicks on
+                // GitRepo::Llvm (d-bootstrap-custom-debuginfo-path.patch), so
+                // only give paths of initial rustc and cargo.
+                Ok(table) => if !cfg!(test) || table.build.is_none() {
+                    table
+                } else {
+                    let mut config = TomlConfig::default();
+                    let mut build = Build::default();
+                    let cbuild = table.build.unwrap();
+                    build.rustc = cbuild.rustc;
+                    build.cargo = cbuild.cargo;
+                    config.build = Some(build);
+                    config
+                },
                 Err(err) => {
                     eprintln!("failed to parse TOML configuration '{}': {}", file.display(), err);
                     crate::detail_exit(2);
